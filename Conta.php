<?php 

class Conta{
    private $nome;
    private $login;
    private $saldo;
    private $tipoConta;
    
    public function __construct($nome, $login, $saldo, $tipodeConta){
        self::setNome($nome);
        self::setLogin($login);
        self::setSaldo($saldo);
        self::setTipoConta($tipodeConta);
    }
    
    
    /**
     * @return mixed
     */
    private function getNome()
    {
        return $this->nome;
    }

    /**
     * @return mixed
     */
    private function getLogin()
    {
        return $this->login;
    }

    /**
     * @return mixed
     */
    private function getSaldo()
    {
        return $this->saldo;
    }

    /**
     * @return mixed
     */
    private function getTipoConta()
    {
        return $this->tipoConta;
    }

    /**
     * @param mixed $nome
     */
    private function setNome($nome)
    {
        $this->nome = $nome;
    }

    /**
     * @param mixed $login
     */
    private function setLogin($login)
    {
        $this->login = $login;
    }

    /**
     * @param mixed $saldo
     */
    private function setSaldo($saldo)
    {
        $this->saldo = $saldo;
    }

    /**
     * @param mixed $tipoConta
     */
    private function setTipoConta($tipoConta)
    {
        $this->tipoConta = $tipoConta;
    }
        
      
    public function mostrarInformacoes(){
        echo "<table style='width:30%''>
              <tr>
                  <th>Nome</th>
                  <th>Login</th>
                  <th>Saldo</th>
                  <th>Tipo de Conta</th>
              </tr>";
        echo "<tr>
            <td>".self::getNome()."</td>;
            <td>".self::getLogin()."</td>;
            <td>".self::getSaldo()."</td>;
            <td>".self::getTipoConta()."</td>;
              </tr>";
    }


}


?>