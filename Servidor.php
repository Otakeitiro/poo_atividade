<?php 

class Servidor extends Conta{
    private $codigo;
    private $cargo;
   
    
   /**
     * @return mixed
     */
    private function getCodigo()
    {
        return $this->codigo;
    }

    /**
     * @return mixed
     */
    private function getCargo()
    {
        return $this->cargo;
    }

    /**
     * @param mixed $codigo
     */
    private function setCodigo($codigo)
    {
        $this->codigo = $codigo;
    }

    /**
     * @param mixed $cargo
     */
    private function setCargo($cargo)
    {
        $this->cargo = $cargo;
    }

    public function sacarServidor(){
        return "Sacar";
        
    }
    
    public function depositarServidor(){
        return "Depositar";
        
    }
   
   public function mostrarInformacoes(){
    echo "<table style='width:30%''>
              <tr>
                  <th>Codigo</th>
                  <th>Cargo</th>
              </tr>";
    echo "<tr>
            <td>".self::getCargo()."</td>;
            <td>".self::getCodigo()."</td>;
              </tr>";
   }
   }
?>               