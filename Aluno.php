<?php 

class Aluno extends Conta{
    private $matricula;
    private $curso;
    
    private function __construct($matricula, $curso){
        self::setCurso($curso);
        self::setMatricula($matricula);
        
        
        
        
    }
    
    
    /**
     * @return mixed
     */
    private function getMatricula()
    {
        return $this->matricula;
    }

    /**
     * @return mixed
     */
    private function getCurso()
    {
        return $this->curso;
    }

    /**
     * @param mixed $matricula
     */
    private function setMatricula($matricula)
    {
        $this->matricula = $matricula;
    }

    /**
     * @param mixed $curso
     */
    private function setCurso($curso)
    {
        $this->curso = $curso;
    }

   
    public function sacarAluno(){
       return "Sacar";
        
    }
    
    public function depositarAluno(){
       return "Depositar";
        
    }
    
    public function mostrarInformacoes(){
        echo "<table style='width:30%''>
              <tr>
                  <th>Matrícula</th>
                  <th>Curso</th>
              </tr>";
        echo "<tr>
            <td>".self::getMatricula()."</td>;
            <td>".self::getCurso()."</td>;
              </tr>";
    }
        
        
    
   
    
    


}


?>